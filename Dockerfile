ARG WORDPRESS_IMAGE=""

FROM $WORDPRESS_IMAGE AS wordpress

FROM nginx:1.15

ARG ENV=""

ARG ENV_USER=""

ARG ENV_PASSWORD=""

RUN apt update && apt install vim -y 

RUN apt install curl -y

RUN if [ ${ENV} != prod_meanwhile_this_condition_is_not_applied ] ; then apt install apache2-utils -y; fi

RUN if [ ${ENV} != prod_meanwhile_this_condition_is_not_applied ] ; then mkdir /etc/apache2 && (echo ${ENV_PASSWORD}; echo ${ENV_PASSWORD}) | htpasswd -c /etc/apache2/.htpasswd ${ENV_USER}; fi

ARG CACHEBUST=1

RUN echo $CACHEBUST;

COPY --from=wordpress /var/www/html /var/www/html

RUN chown -R www-data:www-data /var/www/html

WORKDIR /var/www/html

COPY config/${ENV}.nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080
